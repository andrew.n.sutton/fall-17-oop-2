// (c) Andrew Sutton, PhD
// All rights reserved

#include "card.hpp"

#include <iostream>


int
main()
{
  CardImpl c; // default constructs c
  c.sc = {Nine, Spades}; // sc is the active member of the union.

  SuitedCard sc = c.sc; // OK: reads the active member

  JokerCard jc = c.jc; // error: reads an inactive members
  
  std::cout << jc.color << '\n';

}
