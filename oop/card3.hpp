
#pragma once

#include <iostream>
#include <vector>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Suit
{
  Spades,
  Clubs,
  Hearts,
  Diamonds,
};

enum Color
{
  Black,
  Red,
};

struct Card
{
  void print() const {
    print_ascii_fn(this);
  }

  void print_fancy() const {
    print_fancy_fn(this);
  }

  void print_face() const {
    print_face_fn(this);
  }

  using print_fn = void (*)(const Card*);

  print_fn print_ascii_fn = nullptr;
  print_fn print_fancy_fn = nullptr;
  print_fn print_face_fn = nullptr;
};


struct Suited : Card
{
  Suited(Rank r, Suit s) 
    : rank(r), suit(s) 
  { 
    // Overwrite the the print function.
    print_ascii_fn = print_card;
  }

  static void print_card(const Card* c) {
    const Suited* s = static_cast<const Suited*>(c);
    std::cout << s->rank << ' ' << s->suit << '\n';
  }

  Rank rank;
  Suit suit;
};

struct Joker : Card
{
  Joker(Color c)
    : color(c)
  { 
    print_ascii_fn = print_card;
  }

  static void print_card(const Card* c) {
    const Joker* j = static_cast<const Joker*>(c);
    std::cout << j->color << '\n';
  }

  // void print() const {
  //   std::cout << color << '\n';
  // }

  Color color;
};


using Deck = std::vector<Card*>;
