
#include "json.hpp"

#include <iostream>
#include <iterator>
#include <string>

int 
main() {

  // // [null, true, false, []].
  // Value* v = new Array {
  //   new Null{},
  //   new Bool{true},
  //   new Bool{false},
  //   new Array{}
  // };
  // v->print();

  // // Deleting an object does 2 things.
  // // 1. Calls the destructor on the object a.
  // // 2. Deallocates memory that a occupies.
  // delete v;

  // Read into a string.
  //
  // Copied from StackOverflow:
  //
  // https://stackoverflow.com/questions/2602013/read-whole-ascii-file-into-c-stdstring
  std::string str((std::istreambuf_iterator<char>(std::cin)),
                   std::istreambuf_iterator<char>());

  Value* v = parse(str);
  v->print();
  delete v;
}

