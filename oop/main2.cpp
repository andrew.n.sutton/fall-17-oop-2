// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "card.hpp"

#include <iostream>

void
print(Card* c) {
  if (c->is_joker()) {
    Joker* j = static_cast<Joker*>(c);
    std::cout << j->color << '\n';
  }
  else if (c->is_suited()) {
    Suited* s = static_cast<Suited*>(c);
    std::cout << s->suit << ' ' << s->rank << '\n';
  }
  else {
    throw std::logic_error("invalid card");
  }
}



int main() {

  Spade c1 {0, Ace};
  Spade c2 {1, Two};
  Joker j1 {52, Black};
  Joker j2 {53, Red};

  Deck d;
  d.push_back(&c1);
  d.push_back(&c2);
  d.push_back(&j1);
  d.push_back(&j2);
  random_shuffle(d.begin(), d.end());

  // std::cout << d[3]->id << '\n';
  
  print(d[0]);

  // std::cout << d[3]->color << '\n';
}
