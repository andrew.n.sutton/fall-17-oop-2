// (c) Andrew Sutton, PhD
// All rights reserved

#include <cassert>
#include <utility>
#include <vector>
#include <iosfwd>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

// enum Suit
// {
//   Spades,
//   Clubs,
//   Hearts,
//   Diamonds,
// };

enum Color
{
  Black,
  Red,
};

enum CardType {
  JokerCard,
  SuitedCard,
};


// Represents the set of all (possible) playing cards. Anything can be
// a card as long as it derives from this class.
struct Card
{
  Card(int n, CardType t) 
    : id(n), type(t) 
  { }

  bool is_joker() const {
    return type == JokerCard;
  }

  bool is_suited() const {
    return type == SuitedCard;
  }
  
  int id;
  CardType type;
};

/// A suited (card) IS-A a card.
struct Suited : Card
{
  Suited(int id, Rank r) 
    : Card(id), rank(r)
  { }
  
  Rank rank; // A suited (card) HAS-A rank.
};

// Represents the set of all spade-suited cards.
struct Spade : Suited
{
  // Inherited constructor.
  using Suited::Suited;
};

struct Club : Suited
{
  using Suited::Suited;
};

struct Heart : Suited
{
  using Suited::Suited;
};

struct Diamond : Suited
{
  using Suited::Suited;
};

/// A joker IS-A card.
struct Joker : Card
{
  Joker(int id, Color c) 
    : Card(id), color(c) 
  { }
  Color color;
};


// A deck is a sequence of cards.
using Deck = std::vector<Card*>;

// Can't do this: a reference is not an object.
// using Deck2 = std::vector<Card&>;
// Deck2 d;






