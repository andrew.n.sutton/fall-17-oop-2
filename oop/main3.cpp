// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "card3.hpp"

#include <iostream>

void
print(Card* c) {

}

void
print(const Deck& d) {
  for (Card * c : d) 
    c->print();
}

int main() {

  Deck d {
    new Suited{Ace, Spades},
    new Suited{Two, Spades},
    // ... more cards
    new Joker{Black},
    new Joker{Red},
  };

  print(d);

  // d[0]->print();

  // random_shuffle(d.begin(), d.end());


  // FIXME: Delete cards.
}
