# Notes on the Game of War

We want to know the average length of a game of war. This means we're
going to write a simulation of the game in order to measure game length.
In order to do this we'll have to translate a number of game concepts
into software. Specifically, we will have to include:

- Playing cards
- The initial game deck
- Player hands
- The game logic
- other stuff?

## Representation of cards

In order to define our representation of cards, we need to think about their
properties. Each card has a rank denoted by a set of pips (numbered) or by a face card. An Ace card has a single pip. The face cards are Jacks, Queens,
and Kings.

Each card also has a suit, which is one of:

- hearts
- diamonds
- clubs
- spades

We could choose to represent a card as a pair of integers, by associating 
one integer value with the suit and another with the rank. We could take 
the suit to be the values 0-3, and the rank to be the values 1-13. We might
define this in C++ as an array of 2 elements.

    // A card is an array of two ints.
    // Rank first, suit second (e.g., Queen of hearts).
    using Card = int[2];

This is a [type alias](http://en.cppreference.com/w/cpp/language/type_alias),
defining a `Card` type as an array of two `int`s. A better definition might
be to use a `std::pair`, which avoids some the weirdness of C++ arrays.

    // A card is a pair of two ints.
    // Rank first, suit second.
    using Card = std::pair<int, int>;

However, this isn't a particularly good definition for two reasons. First,
it's probably pretty easy to forget which member of the card represents
the rank and which represents the suit. Second, we've also made it easy
to create Cards objects that don't correspond to actual card values. 
For example:

    Card c1{1, 0}; // OK: Ace (1) of hearts (0)
    Card c1{0, 4}; // OK? What card is this?

In other words, the *abstraction* that we've chosen to represent cards is
extremely *brittle*. Effectively what we've done is started writing our
simulation in a way that makes it easy to make programming mistakes later
on by e.g., forgetting which order we store the rank and suit.


## Value and identity

We briefly touched on the distinction between identity and value today. If
we think about playing the game of war, each card *object* is unique. However,
there may be multiple objects that have the same *value*. For example, if we
play the game with two decks, there will be two cards (objects) for every 
rank and suit (value).

## Comparing cards

We also briefly touched on the notion of comparing cards. In particular,
we noted that aces might compare less than 2's, or they might be greater
than kings. Which depends on the game rules.

## Deck of cards

A deck of cards...


## Game logic
