// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "card4.hpp"

#include <iostream>

void
print(Card* c) {
  // Dynamic dispatch. Find the (virtual) function print
  // in Card. Dispatch to the overridden function for the
  // object.
  c->print();
}

void
print(const Deck& d) {
  for (Card* c : d)
    print(c);
}

int main() {

  Deck d {
    new Suited{Ace, Spades},
    new Suited{Two, Spades},
    // ... more cards
    new Joker{Black},
    new Joker{Red},
    new Special{},
    // new Uno{},
  };

  print(d);

  d[0]->equal(d[1]);

  // d[0]->print();

  // random_shuffle(d.begin(), d.end());


  // FIXME: Delete cards.
}
