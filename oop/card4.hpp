
#pragma once

#include <iostream>
#include <vector>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Suit
{
  Spades,
  Clubs,
  Hearts,
  Diamonds,
};

enum Color
{
  Black,
  Red,
};

struct Card
{
  // // Virtual keyword associates the behavior with the
  // // dynamic type of the object.
  //
  // // A class with at least one virtual is polymorphic.
  // virtual void print() const {
  //   throw std::logic_error("i don't know what you want");
  // }

  // This is a pure virtual function. In C# and Java, this
  // is called an "abstract method".
  //
  // An abstract class has at least one pure virtual 
  // function.
  virtual void print() const = 0;

  // Double dispatch.
  virtual bool equal(const Card* c) const = 0;
  
  virtual bool equal_to(const Card* c) const { return false; }
  virtual bool equal_to(const Suited* c) const { return false; }
  virtual bool equal_to(const Special* c) const { return false; }
  virtual bool equal_to(const Joker* c) const { return false; }
};

bool operator==(const Card& a, const Card& b)
{
  return a.equal(&b);
}

struct Suited : Card
{
  Suited(Rank r, Suit s) 
    : rank(r), suit(s) 
  { }

  // Override the behavior in a derived class. (not overwrite).
  void print() const override {
    std::cout << rank << ' ' << suit << '\n';
  }

  bool equal(const Card* c) const {
    return c->equal_suited(this);
  }

  bool equal_to(const SuitedCard* c) {
    return rank == c->rank && suit == c->suit;
  }

  Rank rank;
  Suit suit;
};

struct Special : Suited
{
  Special()
    : Suited(Ace, Spades)
  { }

  void print() const override {
    std::cout << "special\n";
  }

  bool equal(const Card* c) const {
    return c->equal_to(this);
  }

  bool equal_to(const Special* c) {
    return true;
  }
};

struct Joker : Card
{
  Joker(Color c)
    : color(c)
  { }

  void print() const override {
    std::cout << color << '\n';
  }

  bool equal(const Card* c) const {
    return c->equal_to(this);
  }

  bool equal_to(const Joker* c) {
    return color == c->color;
  }

  Color color;
};

struct Uno : Card
{
  int uno_value; // This is probably not a real thing.
};

struct UnoSuite : Card
{
  void print() const override { }
};


using Deck = std::vector<Card*>;
