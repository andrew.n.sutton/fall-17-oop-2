// (c) Andrew Sutton, PhD
// All rights reserved

#include <cassert>
#include <utility>
#include <iosfwd>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Suit
{
  Spades,
  Clubs,
  Hearts,
  Diamonds,
};


// A playing card (in a standard deck) is a pair of rank and
// suit (see enums above).
struct Card
{
  Card()
    : rank(Ace), suit(Spades)
  { }

  Card(Rank r, Suit s)
    : rank(r), suit(s)
  { }

  Rank rank;
  Suit suit;
};


// Equality comparison
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

// Ordering
bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);

std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);

