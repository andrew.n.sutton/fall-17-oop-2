// (c) Andrew Sutton, PhD
// All rights reserved

#include <cassert>
#include <utility>
#include <iosfwd>

enum Rank
{
  Ace,
  Two,
  Three,
  Four,
  Five,
  Six,
  Seven,
  Eight,
  Nine,
  Ten,
  Jack,
  Queen,
  King,
};

enum Suit
{
  Spades,   // 0
  Clubs,    // 1
  Hearts,   // 2 0b10 (red)
  Diamonds, // 3 0b11 (red)
};

enum Color
{
  Black,
  Red,
};


0b0000'0000


// A card is either a suited card (pair of rank/suit) or a joker (just color).
// If the card is suited, the representation of the card is:
//
//    0b00ss'rrrr
//
// If the card is a joker...
//
//    0b1000'000c
//
// Class invariant: the representation (member variable bits) has either
// the pattern for suited cards or jokers.
//
struct Card
{
  unsigned char bits;

  Card(Rank r, Suit s)
    : bits((s << 4) | r)
  { }

  Card(Color c)
    : bits(0x80 | c)
  { }

  bool is_joker() const {
    return bits & 0x80;
  }

  bool is_suited() const {
    return !is_joker();
  }

  Rank get_rank() const {
    // Assume that invariants.
    assert(is_suited()); // This is a precondtion on the object.
    return (Rank)(bits & 0x0f);
  }

  Suit get_suit() const {
    assert(is_suited());
    return (Suit)(bits >> 4);
  }

  Color get_color() const {
    if (is_joker())
      return (Color)(bits & 0x01);
    else
      return (Color)(get_suit() >= Hearts);
  }

  void set(Rank r, Suit s)
  {
    // Invariants are true here.
    *this = Card(r, s);
    // Invariants must be true here.
  }

};

// Don't do that.
// Card c((Color)42);


// Equality comparison
bool operator==(Card a, Card b);
bool operator!=(Card a, Card b);

// Ordering
bool operator<(Card a, Card b);
bool operator>(Card a, Card b);
bool operator<=(Card a, Card b);
bool operator>=(Card a, Card b);

std::ostream& operator<<(std::ostream& os, Card c);
std::ostream& operator<<(std::ostream& os, Rank r);
std::ostream& operator<<(std::ostream& os, Suit s);

