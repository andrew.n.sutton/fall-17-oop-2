// (c) Andrew Sutton, PhD
// All rights reserved

#include "card.hpp"

#include <cassert>
#include <iostream>
#include <iomanip>
#include <queue>
#include <vector>

using Deck = std::vector<Card>;

using Hand = std::vector<Card>;

void
print(const Deck& deck)
{
  int i = 1;
  for (Card c : deck) {
    std::cout << c << ' ';
    if (i % 13 == 0) {
      std::cout << '\n';
      i = 0;
    }
    ++i;
  }
  std::cout << '\n';
}

void
deal_unchecked(Deck& d, Hand& h)
{
  Card c = d.back();
  d.pop_back();
  h.push_back(c);
}

void
deal_checked(Deck& d, Hand& h)
{
  Deck d0 = d;
  Hand h0 = h;
  deal_unchecked(d, h);
  assert(d.size() == d0.size() - 1);
  assert(std::equal(d.begin(), d.end(), d0.begin()));
  assert(h.size() == h0.size() + 1);
  assert(std::equal(h0.begin(), h0.end(), h.begin()));
  assert(h.back() == d0.back());
}

// Deal one card from the top of d to the 
// front of the hand h. 
//
// Precondition: !d.empty()
//
// narrow contract: behavior is only defined for valid inputs (those
// that satisfy the preconditions).
//
// wide contract: behavior is defined for all (?) inputs, some outputs
// may be error values or exceptions.
//
// effects: If d is (d1, d2, ..., dn, c) and
// and h is (h1, h2, ..., hk), after completion,
// then d is (d1, d2, ..., dn), and
// h is (h1, h2, ..., hk, c).
void
deal_one(Deck& d, Hand& h)
{
  assert(!d.empty());
#ifdef NDEBUG
  deal_unchecked(d, h);
#else
  deal_checked(d, h);
#endif
}

int
main()
{
  Deck d;
  Hand h;

  deal_one(d, h);
  assert(h.size() == 1);

  // Called out of contract...
  deal_one(d, h);
  // Did I deal a card?
}


