// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#pragma once

#include <stdexcept>
#include <iostream>

// This data type represents the set of all expressions,
// as defined by the following set:
//
// e ::= 0 | 1 | 2 | ... | n -- integers
//       e1 + e2             -- addition
//       e1 - e2             -- subtraction
//       e1 * e2             -- multiplication
//       e1 / e2             -- division
//       -e1                 -- negation
//
// Expr is the base class of all expressions.
struct Expr
{
  // Prints a textual representation the expression to an
  // output stream.
  virtual void print(std::ostream& os) const;

  // Computes the value of this expression.
  virtual int evaluate() const;

  // Perform a single-step reduction on this expression.
  virtual Expr* reduce() const;
};

/// Represents expressions of the form 0, 1, 2, ..., n.
struct Int : Expr
{
  Int(int n)
    : val(n)
  { }

  int val;
};

// Kinds of arithmetic operations.
enum Op {
  AddOp,
  SubOp,
  MulOp,
  DivOp,
};

/// Represents all expressions of the form e1 @ e2 where @
/// is one of the operations described by the enum Op.
struct Binary : Expr
{
  Binary(Op op, Expr *e1, Expr* e2)
    : op(op), e1(e1), e2(e2)
  { }

  Op op;
  Expr* e1;
  Expr* e2;
};

/// Represents expressions of the form e1 + e2.
///
/// You *could* keep these classes around.
struct Add : Binary
{
  Add(Expr* e1, Expr* e2)
    : Binary(AddOp, e1, e2)
  { }
};

/// Represents expressions of the form e1 - e2.
struct Sub : Binary
{
  using Binary::Binary;
};

/// Represents expressions of the form e1 * e2.
struct Mul : Binary
{
  using Binary::Binary;
};

/// Represents expressions of the form e1 / e2.
struct Div : Binary
{
  using Binary::Binary;
};

/// Represents expressions of the form -e1.
struct Neg : Expr
{
  Neg(Expr* e1)
    : e1(e1)
  { }

  Expr* e1;
};

