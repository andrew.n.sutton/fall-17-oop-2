// (c) 2017 Andrew Sutton, PhD
// All rights reserved

#include "calc.hpp"

#include <iostream>

std::ostream& 
operator<<(std::ostream& os, const Expr* e)
{
  e->print(os);
  return os;
}

int 
main() {
  // (5 / 2) * (4 + 2)
  Expr* e = new Mul(
    new Div(new Int(5), new Int(2)),
    new Add(new Int(4), new Int(2))
  );

  std::cout << e << " == " << e->evaluate() << '\n';

  Expr* e2 = e->reduce();

  delete e;
  delete e2;

#if 0
  e->compile(std::cout);
  std::cout << '\n';


  // 5 2 / 4 2 + *

  // ipush 5
  // ipush 2
  // idiv
  // ipush 4
  // ipush 2
  // iadd
  // imul


  std::cout << e << " == " << e->evaluate() << '\n';

  while (!e->is_value()) {
    e = e->reduce();
    std::cout << e << '\n';
  }

  // std::cout << e->reduce() << '\n';
  // std::cout << e->reduce()->reduce() << '\n';
#endif
}
