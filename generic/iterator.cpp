
#include <cassert>

template<typename T>
T* 
min_element(T* first, T* last)
{
  T* p = first;
  ++first; // Increment/successor
  while (first != last) {
    if (*first < *p) // *first gets the value
      p =  first;
    ++first;
  }
  return p;
}

struct node
{
  int value;
  node* next;
};

node* 
min_element(node* first, node* last)
{
  node* p = first;
  first = first->next; // advance
  while (first != last) {
    if (first->value < p->value) // first->value gets the value
      p =  first;
    first = first->next;
  }
  return p;
}

template<typename T>
void advance(T*& p)
{
  ++p;
}

template<typename T>
T& get(T* p)
{
  return *p;
}

void advance(node*& p)
{
  p = p->next;
}

int& get(node* p)
{
  return p->value;
}

template<typename I>
I generic_min_element(I first, I last)
{
  I min = first;
  advance(first);
  while (first != last) {
    if (get(first) < get(min))
      min = first;
    advance(first);
  }
  return min;
}


template<typename T>
I 
actual_min_element(I first, I last)
{
  T* p = first;
  ++first; // advance
  while (first != last) {
    if (*first < *p) // get
      p = first;
    ++first;
  }
  return p;
}

/// An iterator adaptor for an underlying node pointer.
struct node_iterator
{
  node_iterator(node* p) : p(p) { }

  node_iterator& operator++()
  {
    p = p->next;
  }

  int& operator*() const 
  { 
    return p->value; 
  }

  bool operator==(node_iterator i) const
  {
    return p == i.p;
  }

  bool operator!=(node_iterator i) const
  {
    return p != i.p;
  }

  // Wraps the underlying node pointer.
  node* p;
};


int main()
{
  int a1[] {1, 0, 3};
  assert(generic_min_element(a1, a1 + 3) == &a1[1]);

  node n1 {3, nullptr};
  node n2 {0, &n1};
  node n3 {1, &n2};
  assert(generic_min_element(&n3, (node*)nullptr) == &n2);
}







