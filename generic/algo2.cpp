
template<typename T>
concept bool EqualityComparable()
{
  return requires (T a, T b) {
    // axiom: a == b is true when a and b represent
    // the same entity. That would guarantee substitutability
    // in regular expressions (i.e., mathy stuff).
    { a == b } -> bool;

    // axiom: a != b <-> !(a == b)
    { a != b } -> bool;
  }
}

template<typename T>
concept bool TotallyOrdered()
{
  return EqualityComparable<T> && requires (T a, T b) {
    // axioms: One of these must be true
    //    a < b
    //    b < a
    //    a == b
    { a < b } -> bool;
    
    // a > b <-> b < a
    { a > b } -> bool;
    
    // a <= b <-> !(b < a)
    { a <= b } -> bool;
    
    // a >= b <-> !(b < a)
    { a >= b } -> bool;
  };
}

struct Color { };

void operator<(Color, Color) { }
void operator>(Color, Color) { }
void operator<=(Color, Color) { }
void operator>=(Color, Color) { }

static_assert(TotallyOrdered<int>());
// static_assert(TotallyOrdered<Color>());

template<typename T>
  requires TotallyOrdered<T>()
T& min(T& a, T& b)
{
  return b < a ? b : a;
}

// template<typename T>
// const T& min(const T& a, const T& b)
// {
//   return b < a ? b : a;
// }

int main()
{
  Color a, b;
  // min(a, b);
}