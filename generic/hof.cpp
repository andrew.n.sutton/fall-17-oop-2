

template<ForwardIterator I, EqualityComparable T>
I find(I first, I last, const T& value)
{
  while (first != last) {
    if (*first == value)
      return first;
  }
  return first;
}

template<typename P, typename... Args>
concept bool Predicate()
{
  return requires (P pred, Args... args) {
    { pred(args...) } -> bool;
  };
}



// You know what I mean.
template<typename P>
concept bool Predicate()
{
  return true;
}

// Function -- unconstrained
// Predicate -- takes some arguments, returns true or false
// Relation -- binary predicate

// equivalence relation
// weak order

// Operators

// Monoid
// Group
// Ring
// Field



template<ForwardIterator I, Predicate P>
I find_if(I first, I last, P pred)
{
  while (first != last) {
    if (pred(*first))
      return first;
  }
  return first;
}

bool
is_negative(int n)
{
  return n < 0;
}

int main()
{
  int a[] {1, 0, -1, -2, 3};

  auto iter = find(a, a + 5, 0);
  assert(iter == a + 1);

  auto iter2 = find_if(a, a + 5, is_negative);
  
  auto iter3 = find_if(a, a + 5, [](int n) { 
    return n < 0;
  });
}



