// (c) Andrew Sutton, PhD
// All rights reserved

#include <iostream>
#include <vector>

// def min(a, b):
//   return a if a < b else b

// function min(a, b) {
//   return a < b ? a : b;
// }

template<typename T>
T min(T a, T b) {
  return a < b ? a : b;
}

// template<>
// int min<int>(int a, int b) {
//   return a < b ? a : b;
// }

struct color {
  float r, g, b;
};

int main()
{
  std::cout << min<int>(0, 3) << '\n';
  std::cout << min<double>(3.14, 2.72) << '\n';

  std::cout << min(0, 3) << '\n';
  std::cout << min(3.14, 2.72) << '\n';

  color red{1,0,0};
  color blue{0,0,1};
  min(red, blue);

  // std::vector<int&> v;
}
