

template<typename T>
concept bool EqualityComparable() {
  return requires (T a, T b) {
    { a == b } -> bool;
    { a != b } -> bool;
  };
}

template<typename T>
concept bool Regular()
{
  return Copyable<T>() && EqualityComparable<T>();
}

template<typename I>
concept bool ForwardIterator()
{
  return Regular<I>() requires (I i) {
    { ++i } -> I&;
    { *i } -> auto&; // Associated type
  };
}

/// This is concept is *refinement* of ForwardIterator.
template<typename I>
concept bool BidirectionalIterator()
{
  return ForwardIterator<I>() && requires (I i) {
    { --i } -> &I;
  };
}

template<typename I>
concept bool RandomAccessIterator()
{
  return TotallyOrdered<I> && 
         BidirectionalIterator<I>() && requires (I i) {
    { i += n } -> &I;
    { i -= n } -> &I;
    { i - i } -> &I;
  };
}


// --------------------------------------------------- //

template<ForwardIterator I>
void advance(I& iter, int n)
{
  // pre: n >= 0
  while (n != 0) { ++iter; --n; }
}

template<BidirectionalIterator I>
void advance(I& iter, int n)
{
  if (n > 0)
    while (n != 0) { ++iter; --n; }
  else if (n < 0)
    while (n != 0) { --iter; ++n; }
}

template<RandomAccessIterator I>
void advance(I& iter, int n)
{
  iter += n;
}


// --------------------------------------------------- //



template<ForwardIterator I>
I 
actual_min_element(I first, I last)
{
  T* p = first;
  ++first; // advance
  while (first != last) { // equality
    if (*first < *p) // get
      p = first; // assignment
    ++first;
  }
  return p;
}


template<InputIterator I>
I find(I first, I last, const T& value)
{
  while (first != last) {
    if (*first == value)
      return first;
    ++first;
  }
  return last;
}






