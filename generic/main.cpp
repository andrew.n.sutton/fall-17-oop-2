// (c) Andrew Sutton, PhD
// All rights reserved

#include <iostream>
#include <vector>


struct pair_int_int {
  int first;
  int second;
};

struct pair_bool_int {
  bool first;
  int second;
};

struct pair_int_bool {
  int first;
  bool second;
};

template< // Declares pair to be a template
  typename T, // T is a template (type) parameter
  typename U // U is a type parameter
  >
struct pair {
  T first;
  U second;
};


#if 0

template<typename T, int N>
struct array
{
  T data[N];
};

array<int, 3> arr;


// This is called the primary template.
template<typename T>
class vector { /* ... */ };

// This declaration results in the implicit instantiation
// of vector<int>.
vector<int> x;

/// This is an explicit instantiation.
template class vector<double>;

// Implicit instantiation generates this. The contents
// of the class are instantiated from the primary template.
template<>
class vector<int> { ... };

// Explicit instantiation generates this. The contents
// of the class are instantiated from the primary template.
template<>
class vector<double> { ... };

/// This is an explicit specialization.
template<>
class vector<bool> {
  // Use a bit-string to store bits.
};

// Partial specialization.
template<typename T>
class vector<T*> {
  // Deletes pointers.
};


vector<bool> vb;
vector<int*> vz;

#endif

int main() {
  // Supplying arguments to a template causes it to be
  // *instantiated*.
  pair< // Pair is a template.
    int, // Int is a template argument
    bool> // bool is a template argument.
  p1;
  // The instantiated type of p1 is below.


  pair<bool, int> p2;


  std::vector<int> vz { 1, 2, 3 };
  for (int& x : vz)
    std::cout << &x << '\n';

  std::vector<bool> vb { 1, 0, 0 };
  // for (bool& x : vb)
  //   std::cout << &x << '\n';


}

// Instantiation generates a specialization. For
// the type pair<int, bool>.
// template<>
// struct pair<int, bool> {
//   int first;
//   bool second;
// }
